;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;General GUI settings;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq auto-save-list-file-prefix nil)

(setq inhibit-splash-screen   t)
(setq ingibit-startup-message t)

(setq inhibit-splash-screen   t)
(setq ingibit-startup-message t)
(setq scroll-step 3)
(set-default 'truncate-lines t)
(setq display-time-24hr-format t)
(display-time-mode	       t)
(size-indication-mode	       nil)
;(tool-bar-mode -1)

(menu-bar-mode -99) ; disable benu bar at all

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;Load paths and particular files;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'load-path "~/.emacs.d/modules");
(add-to-list 'load-path "~/.emacs.d/modules/apel")
(add-to-list 'load-path "~/.emacs.d/modules/color-theme")
(add-to-list 'load-path "~/.emacs.d/modules/company-irony")
(add-to-list 'load-path "~/.emacs.d/modules/company-mode")
(add-to-list 'load-path "~/.emacs.d/modules/dash")
(add-to-list 'load-path "~/.emacs.d/modules/demangle-mode")
(add-to-list 'load-path "~/.emacs.d/modules/flim")
(add-to-list 'load-path "~/.emacs.d/modules/flycheck")
(add-to-list 'load-path "~/.emacs.d/modules/flycheck-irony")
(add-to-list 'load-path "~/.emacs.d/modules/haskell-mode")
(add-to-list 'load-path "~/.emacs.d/modules/irony")
(add-to-list 'load-path "~/.emacs.d/modules/irony-eldoc")
(add-to-list 'load-path "~/.emacs.d/modules/semi")
(add-to-list 'load-path "~/.emacs.d/modules/seq")
(add-to-list 'load-path "~/.emacs.d/modules/undo-tree")
(add-to-list 'load-path "~/.emacs.d/modules/wanderlust/elmo")
(add-to-list 'load-path "~/.emacs.d/modules/wanderlust/wl")

(load "~/.emacs.d/modules/sr-speedbar.el")
(load "~/.emacs.d/modules/diff-mode-.el")
(load "~/.emacs.d/modules/idle-highlight-mode.el")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;PACKAGE;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'package)
(push '("marmalade" . "http://marmalade-repo.org/packages/") package-archives )
(push '("melpa" . "http://melpa.milkbox.net/packages/") package-archives)

(package-initialize)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;SMALL PACKAGES SETTINGS;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; UNDO TREE MODE
(require 'undo-tree)
(global-undo-tree-mode) ; enable globally

; COLOR THEME
(require 'color-theme)
(eval-after-load "color-theme"
  '(progn
     (color-theme-initialize)
     (color-theme-hober)))
(color-theme-calm-forest) ; set the particula color theme

; HL-LINE
(require 'hl-line)
(set-face-background hl-line-face "color-236") ; highlight current line

; SR-SPEEDBAR
(require 'sr-speedbar)

; IDO MODE
(require 'ido)
(ido-mode t)
(setq ido-enable-prefix nil
      ido-enable-flex-matching t
      ido-create-new-buffer 'always
      ido-use-filename-at-point t
      ido-max-prospects 10)
(setq ido-save-directory-list-file nil)

; PAREN
(require 'paren)
(show-paren-mode 1)
(setq show-paren-style 'expression)
(setq show-paren-delay 0)
(set-face-background 'show-paren-match "#444444")

; LINUM
; [[https://www.emacswiki.org/emacs/LineNumbers]]
(require 'linum)
(setq linum-format "%4d \u2502 ")

; IMENU
(require 'imenu)
(setq imenu-auto-rescan	     t)

; FOLGING
(autoload 'folding-mode		 "folding" "Folding mode" t)
(autoload 'turn-off-folding-mode "folding" "Folding mode" t)
(autoload 'turn-on-folding-mode	 "folding" "Folding mode" t)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;FILE ASSOCIATIONS;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'auto-mode-alist '("\\makefile_new\\'" . makefile-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;TABBAR-MODE;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(global-set-key (kbd "C-c <down>") 'tabbar-backward-group)
(global-set-key (kbd "C-c <up>") 'tabbar-forward-group)
(global-set-key (kbd "C-c <left>") 'tabbar-backward-tab)
(global-set-key (kbd "C-c <right>") 'tabbar-forward-tab)

(defun tabbar-buffer-groups ()
   "Return the list of group names the current buffer belongs to.
     Return a list of one element based on major mode."
   (list
    (cond
     ((or (get-buffer-process (current-buffer))
	  ;; Check if the major mode derives from `comint-mode' or
	  ;; `compilation-mode'.
	  (tabbar-buffer-mode-derived-p
	   major-mode '(comint-mode compilation-mode)))
      "Process"
      )
     ((member (buffer-name)
	       '("*scratch*" "*Messages*" "*Help*"))
       "Common"
     )
     ((string-equal "*" (substring (buffer-name) 0 1))
      "Common"
      )
     ((eq major-mode 'dired-mode)
      "Dired"
      )
     ((memq major-mode
	    '(help-mode apropos-mode Info-mode Man-mode))
      "Common"
      )
     (t
      ;; Return `mode-name' if not blank, `major-mode' otherwise.
      (if (and (stringp mode-name)
	       ;; Take care of preserving the match-data because this
	       ;; function is called when updating the header line.
	       (save-match-data (string-match "[^ ]" mode-name)))
	  (replace-regexp-in-string "\+\+" "" mode-name ) ; mix c++ and c buffers to one group
	(symbol-name major-mode))
      ))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(setq custom-file "~/.emacs-custom.el")
(load custom-file)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 ;; '(tabbar-mode t nil (tabbar)))
 '(initial-scratch-message "")
 )


(put 'upcase-region 'disabled nil)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;COMPANY/IRONY;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(load "~/.emacs.d/modules/let-alist-1.0.3.el")

(require 'company)
(require 'irony)
(require 'irony-cdb)
(require 'company-irony)
(require 'eldoc)
(require 'irony-eldoc)
(require 'flycheck)
(require 'flycheck-irony)

(eval-after-load 'flycheck
      '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))

;; replace the `completion-at-point' and `complete-symbol' bindings in
;; irony-mode's buffers by irony-mode's function
;; Snippet from here: [[https://github.com/Sarcasm/irony-mode]]
(defun my-irony-mode-hook ()
  (define-key irony-mode-map [remap completion-at-point]
    'irony-completion-at-point-async)
  (define-key irony-mode-map [remap complete-symbol]
    'irony-completion-at-point-async))

(add-hook 'irony-mode-hook 'my-irony-mode-hook)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(eval-after-load 'company
      '(add-to-list 'company-backends 'company-irony))


(global-set-key (kbd "C-h a") 'apropos)

(global-set-key "\M-j" 'join-line)

(global-set-key "\M-l" 'goto-line) ;
(global-set-key "\M-h" 'highlight-phrase);


(global-set-key (kbd "C-x C-g") 'imenu)

(global-set-key (kbd "<select>") 'move-end-of-line)

(global-set-key (kbd "C-<tab>") 'company-complete) ; works only for GUI Emacs
(global-set-key (kbd "C-c s")	'company-complete)
(global-set-key (kbd "C-c c")	'sm/c++)
(global-set-key (kbd "C-c o")	'occur)
(global-set-key (kbd "C-c +")	'sm/c++write)
(global-set-key (kbd "C-c g")	'sm/general)
(global-set-key (kbd "C-c q")	'sr-speedbar-toggle)
(global-set-key (kbd "C-c o")	'ff-find-other-file)
(global-set-key (kbd "C-c w")	'sm/toggle-trailing-whitespaces)
(global-set-key (kbd "C-c j")	'sm/mail-mode)
(global-set-key (kbd "C-c e")	'read-only-mode)
(global-set-key (kbd "C-c r")	'ff-find-related-file)

(global-set-key (kbd "C-c m g")	  'sm/graphviz-mode)
(global-set-key (kbd "C-c m v")	  'sm/vb-mode)
(global-set-key (kbd "C-c m i")	  'sm/ini-mode)
(global-set-key (kbd "C-c m w")	  'sm/whitespaces-mode)
(global-set-key (kbd "C-c m l")	  'sm/llvm-mode)

(global-set-key (kbd "ESC <down>") 'sm/next-10-lines)
(global-set-key (kbd "<M-down>") 'sm/next-10-lines)
(global-set-key (kbd "ESC <up>") 'sm/prev-10-lines)
(global-set-key (kbd "<M-up>") 'sm/prev-10-lines)


(defun sm/general
    () (interactive)
    (hl-line-mode)
    (linum-mode)
    (idle-highlight-mode)
    (read-only-mode)
    (setq show-trailing-whitespace 1))

(defun sm/c++
    () (interactive)
    (company-mode)
    (irony-mode)
    (idle-highlight-mode)
    (linum-mode)
    (subword-mode)
    (read-only-mode))

(defun sm/c++write
    () (interactive)
    (eldoc-mode)
    (hl-line-mode)
    (irony-eldoc)
    (flycheck-mode)
    (read-only-mode))

(defun sm/toggle-trailing-whitespaces
    () "" (interactive)
    (setq show-trailing-whitespace (not show-trailing-whitespace)))

(defun sm/mail-mode
    () "" (interactive)
    (require 'wl)
    (wl-mode))

(defun sm/whitespaces-mode
    () (interactive)
    (whitespace-mode))

(defun sm/graphviz-mode
    () (interactive)
    (load "~/.emacs.d/modules/graphviz-dot-mode.el")
    (graphviz-dot-mode))

(defun sm/vb-mode
    () (interactive)
    (load "~/.emacs.d/modules/vb-mode.el")
    (autoload 'visual-basic-mode "visual-basic-mode" "Visual Basic mode." t)
    (setq auto-mode-alist (append
	'(("\\.\\(frm\\|bas\\|cls\\)$" . visual-basic-mode)) auto-mode-alist))
    (visual-basic-mode))

(defun sm/ini-mode
    () (interactive)
    (load "~/.emacs.d/modules/ini-mode.el")
    (require 'ini-mode)
    (ini-mode))

(defun sm/llvm-mode
    () (interactive)
    (load "~/.emacs.d/modules/llvm-mode.el")
    (require 'llvm-mode)
    (llvm-mode))

(defun sm/next-10-lines
    () (interactive)
    (next-line 10))

(defun sm/prev-10-lines
    () (interactive)
    (previous-line 10))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;CYRILLIC;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun cfg:reverse-input-method (input-method)
  "Build the reverse mapping of single letters from INPUT-METHOD."
  (interactive
   (list (read-input-method-name "Use input method (default current): ")))
  (if (and input-method (symbolp input-method))
      (setq input-method (symbol-name input-method)))
  (let ((current current-input-method)
        (modifiers '(nil (control) (meta) (control meta))))
    (when input-method
      (activate-input-method input-method))
    (when (and current-input-method quail-keyboard-layout)
      (dolist (map (cdr (quail-map)))
        (let* ((to (car map))
               (from (quail-get-translation
                      (cadr map) (char-to-string to) 1)))
          (when (and (characterp from) (characterp to))
            (dolist (mod modifiers)
              (define-key local-function-key-map
                (vector (append mod (list from)))
                (vector (append mod (list to)))))))))
    (when input-method
      (activate-input-method current))))

(cfg:reverse-input-method 'russian-computer)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;ORG MODE;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(require 'org-install)
(setq org-log-done t)
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(fset 'yes-or-no-p 'y-or-n-p)
(setq org-directory "~/org")
(setq org-todo-state-tags-triggers
      (quote (("CANCELLED" ("CANCELLED" . t))
	      ("BLOCKED" ("BLOCKED" . t))
	      ("HOLD" ("BLOCKED") ("HOLD" . t))
	      (done ("BLOCKED") ("HOLD"))
	      ("TODO" ("BLOCKED") ("CANCELLED") ("HOLD"))
	      ("MEETING" ("CANCELLED") ("HOLD"))
	      ("DONE" ("BLOCKED") ("CANCELLED") ("HOLD")))))
(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
	      ("MEETING" :foreground "blue" :weight bold)
	      ("DONE" :foreground "forest green" :weight bold)
	      ("BLOCKED" :foreground "orange" :weight bold)
	      ("HOLD" :foreground "magenta" :weight bold)
	      ("CANCELLED" :foreground "forest green" :weight bold))))
(setq org-log-done (quote time))
(setq org-log-into-drawer t)
(setq org-log-state-notes-insert-after-drawers nil)
(setq org-agenda-files (file-expand-wildcards "~/org_notes/*.org"))

(setq org-todo-keywords
      '(
	(sequence "TODO" "BLOCKED" "HOLD" "MEETING" "|" "DONE" "CANCELLED")
	)
      )
(setq org-tag-alist
      '(
	("HOME" . ?h)
	("CODING" . ?c)
	("WRITING" . ?w)
	("FUN" . ?f)
	("MD" . ?m)
	("YANDEX" . ?y)
	("INTEL" . ?i)
	("FREELANCE" . ?f)
	("MIREA")
	("REPORT" . ?r)
	)
      )

(custom-set-variables
 '(org-time-stamp-custom-formats (quote ("<%d.%m.%Y>" . "<%Y-%m-%d %H:%M>"))))


(setq org-agenda-custom-commands
      '(("R" "Agenda"
	 (
	  (agenda "TODAY" ((org-agenda-ndays 1)
			   (org-deadline-warning-days 7)
			   (org-agenda-todo-keyword-format "")
			   (org-agenda-prefix-format "	* %? e")
			   (org-agenda-sorting-strategy '(effort-up priority-down))
			   (org-agenda-time-grid nil)
			   (org-agenda-remove-tags t)
			   (org-agenda-skip-function (quote (org-agenda-skip-entry-if 'todo '("DONE" "CANCELLED")) ) )
			   (org-agenda-overriding-header "<<Today>>\n")
			   ))
	  (agenda "" (
		      (org-agenda-start-on-weekday 1)
		      (org-agenda-repeating-timestamp-show-all t)
		      (org-agenda-time-grid nil)
		      (org-agenda-remove-tags t)
		      (org-agenda-todo-keyword-format "")
		      (org-agenda-prefix-format "  * %? e")
		      (org-agenda-sorting-strategy '(effort-up priority-down))
		      (org-agenda-skip-function (quote (org-agenda-skip-entry-if 'todo '("DONE" "CANCELLED")) ) )
		      (org-agenda-overriding-header "\n<<Week>>\n")
		      ))
	  (todo "TODO"
		((org-agenda-prefix-format "  * [ ] %? e")
		 (org-agenda-sorting-strategy '(tag-up priority-))
		 (org-agenda-remove-tags t)
		 (org-agenda-todo-keyword-format "")
		 (org-agenda-sorting-strategy '(effort-up priority-down))
		 (org-agenda-skip-function (quote (org-agenda-skip-entry-if 'todo '("DONE" "CANCELLED")) ) )
		 (org-agenda-overriding-header "\n<<All TODOs>>\n"))))
	 ((org-agenda-with-colors t)
	  (org-agenda-compact-blocks nil)
	  (org-agenda-columns)
	  )
	 )
	)
      )

(setq org-columns-default-format "%5TODO %1PRIORITY(P) %4Effort(EFFORT) %15DEADLINE %80ITEM")

(add-hook 'org-finalize-agenda-hook
	  (lambda ()
	    (save-excursion
	      (color-org-header "<<Today>>"  "gold")
	      (color-org-header "<<Week>>" "gold")
	      (color-org-header "<<All TODOs>>" "gold")
	      )))

(defun color-org-header (tag col)
  ""
  (interactive)
  (goto-char (point-min))
  (while (re-search-forward tag nil t)
    (add-text-properties (match-beginning 0) (point-at-eol)
			 `(face (:foreground ,col)))))
