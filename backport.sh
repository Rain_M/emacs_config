#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/"

echo "Copy .bash_profile"
cp ~/.bash_profile $DIR.bash_profile

echo "Copy .bashrc"
cp ~/.bashrc $DIR.bashrc

echo "Copy .emacs"
cp ~/.emacs $DIR.emacs

echo "Copy .emacs-custom.el"
cp ~/.emacs-custom.el $DIR.emacs-custom.el

echo "Copy .emacs.d directory"
mkdir -p $DIR.emacs.d/
cp -r ~/.emacs.d/*  $DIR.emacs.d/

echo "Copy .minttypc"
cp ~/.minttypc $DIR.minttypc

echo "Copy .minttyrc"
cp ~/.minttyrc $DIR.minttyrc

echo "Copy .tmux.conf"
cp ~/.tmux.conf $DIR.tmux.conf

echo "Copy .folders"
cp ~/.folders $DIR.folders

echo "Done!"
