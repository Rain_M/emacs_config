# README #

Sergey Melnikov's emacs config

### What is this repository for? ###

* The aim for this repo is to have unified EMACS-environment across all OSs

## Features ##

* undo tree
* hl-line
* ediff
* conmany-mode
* irony-mode
* wanderlust
* ...

## Who do I talk to? ##

* Repo owner or admin