#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/"

echo "Making ST"
make -C $DIR/st

echo "Copy st/st"
mkdir -p ~/.local/bin/
cp $DIR/st/st ~/.local/bin/

echo "Copy st icon"
cp $DIR/st.desktop ~/.local/share/applications/st.desktop

echo "Copy .bash_profile"
cp $DIR.bash_profile ~/.bash_profile

echo "Copy .bashrc"
cp $DIR.bashrc ~/.bashrc

echo "Copy .emacs"
cp $DIR.emacs ~/.emacs

echo "Copy .emacs-custom.el"
cp $DIR.emacs-custom.el ~/.emacs-custom.el

echo "Copy .emacs.d directory"
mkdir -p ~/.emacs.d
cp -r $DIR.emacs.d/* ~/.emacs.d/

echo "Copy .minttyrc"
cp $DIR.minttyrc ~/.minttyrc

echo "Copy .minttypc"
cp $DIR.minttypc ~/.minttypc

echo "Copy .tmux.conf"
cp $DIR.tmux.conf ~/.tmux.conf

echo "Copy .folders"
cp $DIR.folders ~/.folders

echo "Done!"
